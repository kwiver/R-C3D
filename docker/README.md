# DockerFile for RC3D

The folder contains the docker file to build RC3D with DIVA

## Dependencies
1. [Docker CE](https://docs.docker.com/install/linux/docker-ce/ubuntu/)
2. [Nvidia Docker 2](https://github.com/NVIDIA/nvidia-docker)

## Build
    sudo nvidia-docker build -t kitware/rc3d .

## Run The Container
    sudo nvidia-docker run -ti --name rc3d \
          --mount type=bind,source=<path to test json directory>,target=/data/ \
          --mount type=bind,source=<path to directory where output visualization would be stored>,target=/experiment/output \
                        kitware/rc3d:latest

### Note: modify the json directory path to a directory with activity-index.json provided by NIST

### Note: Further instructions related to virat based experiments are available in experiments/virat folder in RC3D root
