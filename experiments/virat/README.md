# MEVA Based Experiments

## Content
1. [Prerequisites](#prerequisites)
2. [Installation](#installation)
3. [Setup](#setup)
4. [Reproducing Baseline Results](#reproducing-baseline-results)
5. [Usage](#usage)
6. [Options](#options)


##  Prerequisites
1. [diva-framework](https://pypi.org/project/diva-framework)
3. [CUDA 8](https://developer.nvidia.com/cuda-80-ga2-download-archive)
4. [CUDNN 6](https://developer.nvidia.com/cudnn)
5. [RC3D Dependencies](https://github.com/VisionLearningGroup/R-C3D)

### Note we call the directory where you cloned RC3D as $RC3D\_ROOT

## Installation
1. [Fletch](https://github.com/Kitware/fletch) in provides the dependencies to build
   3D-caffe. This includes Boost, Eigen, GLog, HDF5, OpenBlas, Protobuf, Snappy.
   We would refer to the fletch build directory as $FLETCH\_ROOT.
2. To build caffe use the following commands

    ```Shell
    cd RC3D_ROOT/caffe3d
    mkdir  build && cd build
    cmake -DCMAKE_PREFIX_PATH:PATH=${FLETCH_ROOT} -DBLAS=Open -Dpython_version=3 ../
    make
    ```

## Setup
1. Modify $PATH and $PYTHONPATH by using the setup script

    ```Shell
    source ${RC3D_ROOT}/setup_RC3D.sh
    ```
2. The training uses a pretrained model C3D model trained on ActivityNet. It is provided [here](https://drive.google.com/file/d/131Cpuq1FndydeHzu38TY0baiS-uyN71w/view). 

    ```Shell
    mkdir RC3D_ROOT/experiments/virat/pretrain
    wget -O RC3D_ROOT/experiments/virat/pretrain/ucf101.caffemodel \
            https://data.kitware.com/api/v1/file/5b4e638f8d777f2e6225b907/download 
    ```
3. The training, testing and scoring code relies on yml configuration files for parameters. Modify experiment.yml with appropriate parameters. (All the experiment parameters are present in $RC3D\_ROOT/lib/tdcnn/exp\_config.py)



### Using Existing Models
1. For sequestered evaluation, we use [Actev SDL](https://actev.nist.gov/sdl) and submit RC3D using [diva_evaluation_cli](https://gitlab.kitware.com/kwiver/diva_evaluation_cli).

### Using Your Own Model
1.  To retrain the model using the main\_wrapper use the following

	```Shell
	cd $RC3D_ROOT
	python experiments/virat/main_wrapper.py \
          --exp experiments/virat/experiment.yml \
          --model_cfg experiment/virat/tdcnn_end2end.yml
	```

## Usage
	python main_wrapper.py --options


## Options
	--exp  experiment configuration used to specify model parameters
	--skip_train flag to skip training (default=False)
	--skip_test  flag to skip testing (default=False)
    --use_json_class_index  Use json index provided by NIST
    --dump_individual Create json activities file for every video
    --test-activity-index Activity index use for training and evaluation (provided in RC3D-ROOT/activity-index)
    --chunk_id  Chunk id used for testing
