#!/usr/bin/env python

# --------------------------------------------------------
# R-C3D
# Copyright (c) 2017 Boston Univ.
# Licensed under The MIT License [see LICENSE for details]
# Written by Huijuan Xu
# --------------------------------------------------------


"""Set up paths for R-C3D."""

import os.path as osp
import sys

def add_path(path):
    if path not in sys.path:
        sys.path.insert(0, path)

this_dir = osp.dirname(__file__)
# Add caffe to PYTHONPATH
add_path(osp.join(this_dir, '..', '..', 'lib'))
add_path(osp.join(this_dir, '..', '..', 'caffe3d', 'python'))
add_path(osp.join(this_dir, '..', '..', 'preprocess', 'virat'))

