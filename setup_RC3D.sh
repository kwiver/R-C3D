#+
# Setup script for RC3D
# Usage:
# source setup_RC3D.sh
SCRIPT_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
export PYTHONPATH=${SCRIPT_DIR}/lib:${SCRIPT_DIR}/caffe3d/python:$PYTHONPATH
export PYTHONPATH=${SCRIPT_DIR}/experiments/virat:${SCRIPT_DIR}:${PYTHONPATH}
export PYTHONPATH=${SCRIPT_DIR}/preprocess/virat:$PYTHONPATH
export SPROKIT_PYTHON_MODULES=processes:$SPROKIT_PYTHON_MODULES
