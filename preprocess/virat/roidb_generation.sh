python roidb_generation.py --kpf_mode --window_length 768 \
    --window_divisor 4 --min_length 3  --sampling_frequency 8 \
    --data_root /data/diva --frames_root v1-frames \
    --annotation_dirs drop-4-kpf/train-kpf drop-4-kpf/validate-kpf \
    --class_index VIRAT-V1_activity-index.txt \
    --pkl_file kpf_roidb_train.pkl
