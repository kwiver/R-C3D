python roidb_generation.py --test_mode --window_length 768 \
    --window_divisor 4 --min_length 3  --sampling_frequency 8 \
    --data_root /data/diva --frames_root v1-frames \
    --annotation_dirs drop4-activity-density/test \
    --class_index VIRAT-V1_activity-index.txt \
    --pkl_file density_roidb_test.pkl
