python roidb_generation.py --test_mode --kpf_mode --window_length 768 \
    --window_divisor 4 --min_length 3  --sampling_frequency 8 \
    --data_root /data/diva --frames_root v1-frames \
    --json_index_path nist-json/file-index.json \
    --pkl_file kpf_roidb_test.pkl
