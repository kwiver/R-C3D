"""
This is the default experiment configuration for virat dataset. These values should
not be altered instead write a config file (in yaml) to override the default option.

Check sample_exp.yaml for a sample configuration file
"""
from easydict import EasyDict as edict
import os
import yaml
from tdcnn.config import _merge_a_into_b


experiment_config = edict()
# General parameters
experiment_config.gpu                            = 0
experiment_config.kpf                            = True
experiment_config.use_flipped                    = False
experiment_config.json                           = False
experiment_config.frame_roots                    = ['/data/diva/v1-frames/']
experiment_config.class_index                    = '/data/diva/VIRAT-V1_activity-index.txt'
experiment_config.experiment_root                = '/data/diva/experiments/virat'
experiment_config.cache_root                     = '/data/diva/system-cache'
experiment_config.results_path                   = 'results'
experiment_config.model_root                     = '/diva/R-C3D/experiments/virat'
# Training parameters
experiment_config.train = edict()
experiment_config.train.pickle_output            = 'train_pickle.pkl'
experiment_config.train.kpf_annotation_dirs      = ['/data/diva/drop4-activity-density/train/', '/data/diva/drop4-activity-density/validate/']
experiment_config.train.density_annotation_dirs  = ['/data/diva/drop4-activity-density/train/', '/data/diva/drop4-activity-density/validate/']
experiment_config.train.json_annotations     = ['/data/diva/nist-json/eval_1.B/VIRAT-V1_JSON_train-leaderboard_drop4_20180614', '/data/diva/nist-json/eval_1.B/VIRAT-V1_JSON_validate-leaderboard_drop4_20180614']
experiment_config.train.solver                   = 'solver.prototxt'
experiment_config.train.iterations               = 60000
experiment_config.train.restore_iteration       = 0
experiment_config.train.weights                  = ''
experiment_config.train.seed                     = 2000
# Testing parameters
experiment_config.test = edict()
experiment_config.test.pickle_output             = 'test_pickle.pkl'
experiment_config.test.file_index                = 'nist-json/file-index.json'
experiment_config.test.chunk_file                = ''
experiment_config.test.network                   = 'test.prototxt'
experiment_config.test.model                     = ''
experiment_config.test.max_detections            = 100
experiment_config.test.visualize                 = False

def expcfg_from_file(filename):
    #Load a config file and merge it into the default options.
    with open(filename, 'r') as f:
        yaml_cfg = edict(yaml.load(f))
    _merge_a_into_b(yaml_cfg, experiment_config)
