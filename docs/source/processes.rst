KWIVER Processes in RC3D
========================

RC3D :cite:`Xu2017iccv`

.. autoclass:: rc3d_detector.RC3DDetector 

.. autoclass:: rc3d_json_writer.RC3DJsonWriter

.. autoclass:: rc3d_visualizer.RC3DVisualizer

.. bibliography:: processes.bib
