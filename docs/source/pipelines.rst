Pipelines in RC3D
=================

RC3D Pipeline
-------------

.. graphviz:: _pipe/rc3d.dot

RC3D ZMQ Pipeline
-----------------

.. graphviz:: _pipe/rc3d_zmq.dot


