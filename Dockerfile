FROM kitware/fletch:ubuntu16_latest

# Remove the caffe bits and pieces from fletch
RUN rm -r /fletch_install/include/*caffe* \
    && rm -r /fletch_install/lib/*caffe*

# TODO: Build of master after #823 is merged
RUN git clone https://github.com/Kitware/kwiver.git /kwiver \
    && mkdir /kwiver_install \
    && mkdir -p /kwiver/build/release \
    && cd /kwiver  \
    && git checkout dev/fix-python-peek-at-datum \
    && cd build/release \
    && cmake ../../ -DCMAKE_BUILD_TYPE=Release \
       -Dfletch_DIR:PATH=/fletch_install/share/cmake \
       -DCMAKE_INSTALL_PREFIX:PATH=/kwiver_install \
       -DKWIVER_ENABLE_ARROWS=ON \
       -DKWIVER_ENABLE_C_BINDINGS=ON \
       -DKWIVER_ENABLE_CERES=ON \
       -DKWIVER_ENABLE_EXTRAS=ON \
       -DKWIVER_ENABLE_EXAMPLES=ON \
       -DKWIVER_ENABLE_ZeroMQ:BOOL=ON \
       -DKWIVER_INSTALL_SET_UP_SCRIPT:BOOL=ON \
       -DKWIVER_ENABLE_LOG4CPLUS=ON \
       -DKWIVER_ENABLE_OPENCV=ON \
       -DKWIVER_ENABLE_PROCESSES=ON \
       -DKWIVER_ENABLE_PROJ=OFF \
       -DKWIVER_ENABLE_PYTHON=ON \
       -DKWIVER_ENABLE_SPROKIT=ON \
       -DKWIVER_ENABLE_TOOLS=ON \
       -DKWIVER_ENABLE_VXL=ON \
       -DKWIVER_ENABLE_TRACK_ORACLE:BOOL=ON \
    && make -j$(nproc) \
    && make install

# TODO: Build with master after cli-helpers are merged
RUN git clone https://github.com/Kitware/DIVA.git /diva \
    && mkdir /diva_install \
    && mkdir -p /diva/build/release \
    && cd /diva \
    && git fetch origin && git checkout dev/add-cli-helper-scripts \
    && cd /diva/build/release \
    && cmake ../../ -DCMAKE_BUILD_TYPE=Release \
       -DCMAKE_INSTALL_PREFIX:PATH=/diva_install \
       -DDIVA_ENABLE_PYTHON:BOOL=ON \
       -DDIVA_SUPERBUILD:BOOL=OFF \
       -DDIVA_ENABLE_PROCESSES:BOOL=ON \
       -DDIVA_PYTHON_MAJOR_VERSION=2 \
       -Dfletch_DIR:PATH=/fletch_install/share/cmake \
       -Dkwiver_DIR:PATH=/kwiver_install/lib/cmake/kwiver \
    && make \
    && make install

FROM nvidia/cuda:8.0-cudnn6-devel-ubuntu16.04

RUN apt-get update \
    && apt-get install -y --no-install-recommends \
                    build-essential \
                    libgl1-mesa-dev \
                    libexpat1-dev \
                    libgtk2.0-dev \
                    liblapack-dev \
                    python2.7-dev \
                    git \
                    wget \
                    python-dev \
                    python-numpy \
                    python-setuptools \
                    python-scipy \
                    libssl-dev  \
    && easy_install pip && pip install pip --upgrade \
    && mkdir /fletch_install \
    && mkdir /kwiver_install \
    && mkdir /diva_install 
#    && ln -sf /usr/local/cuda/lib64/stubs/libcuda.so /usr/local/cuda/lib64/libcuda.so

RUN wget -O cmake-3.12.3.tar.gz https://cmake.org/files/v3.12/cmake-3.12.3.tar.gz && \
    tar xvzf cmake-3.12.3.tar.gz && cd cmake-3.12.3 && \
    ./bootstrap && make -j$(nproc) && make install

COPY --from=0 /fletch_install /fletch_install
COPY --from=0 /kwiver_install /kwiver_install
COPY --from=0 /diva_install /diva_install

ENV DIVA_INSTALL=/diva_install
# Setup_kwiver env variables
ENV VG_PLUGIN_PATH=${DIVA_INSTALL}
ENV PATH=${DIVA_INSTALL}/bin:${PATH}
ENV LD_LIBRARY_PATH=${DIVA_INSTALL}/lib:${LD_LIBRARY_PATH}
ENV KWIVER_PLUGIN_PATH=${DIVA_INSTALL}/lib/kwiver/modules:${DIVA_INSTALL}/lib/kwiver/processes:${KWIVER_PLUGIN_PATH}
# Append here
ENV VITAL_LOGGER_FACTORY=${DIVA_INSTALL}/lib/kwiver/modules/vital_log4cplus_logger
ENV LOG4CPLUS_CONFIGURATION=${DIVA_INSTALL}/log4cplus.properties
# Python environment
ENV PYTHON_LIBRARY="/usr/lib/x86_64-linux-gnu/libpython2.7.so"
ENV PYTHONPATH=${DIVA_INSTALL}/lib/python2.7/dist-packages:${PYTHONPATH}
ENV PYTHONPATH=${DIVA_INSTALL}/lib/site-packages:${PYTHONPATH}
ENV SPROKIT_PYTHON_MODULES=kwiver.processes
# Setup_diva env variables
ENV PYTHONPATH=${DIVA_INSTALL}/lib:${PYTHONPATH}
ENV KWIVER_PLUGIN_PATH=${DIVA_INSTALL}/lib/diva/processes:${DIVA_INSTALL}/lib/diva/modules:${KWIVER_PLUGIN_PATH}
ENV SPROKIT_PYTHON_MODULES=DIVA.processes:${SPROKIT_PYTHON_MODULES}

# Setup RC3D
ENV RC3D_ROOT=/R-C3D
RUN git clone https://gitlab.kitware.com/kwiver/R-C3D.git $RC3D_ROOT \
    && cd $RC3D_ROOT \
    && git checkout dev/online-rc3d \
    && cd caffe3d/python \
    && for req in $(cat requirements.txt) pydot easydict jsonschema munkres; do pip install $req; done \
    && cd .. \
    && mkdir build && cd build \
    && cmake ../ -DCMAKE_PREFIX_PATH:PATH=/fletch_install \
             -DBLAS=Open \
             -DCUDA_ARCH_NAME=Manual \
             -DCUDA_ARCH_BIN="52, 60, 61" -DCUDA_ARCH_PTX="52, 60, 61" \
    && make \
    && cd $RC3D_ROOT/lib \
    && make \
    && mkdir -p $RC3D_ROOT/experiments/virat/pretrain \
    && wget -O $RC3D_ROOT/experiments/virat/pretrain/ucf101.caffemodel \
        https://data.kitware.com/api/v1/file/5b47c5178d777f2e6225a757/download \
    && mkdir -p $RC3D_ROOT/experiments/virat/results/snapshot \
    && wget -O $RC3D_ROOT/experiments/virat/results/snapshot/virat_iter_60000.caffemodel \
                https://data.kitware.com/api/v1/item/5c38dcab8d777f072b008a76/download 

ENV PYTHONPATH=${RC3D_ROOT}/experiments/virat:${RC3D_ROOT}:${PYTHONPATH}
ENV SPROKIT_PYTHON_MODULES=processes:$SPROKIT_PYTHON_MODULES

WORKDIR ${RC3D_ROOT}
ENTRYPOINT [ "pipeline_runner", "-p" , "pipelines/rc3d.pipe" ]



