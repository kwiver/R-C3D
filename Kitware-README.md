# Adapting R-C3D to the DIVA Framework

This is a fork of [R-C3D: Region Convolutional 3D Network for Temporal Activity Detection](https://github.com/VisionLearningGroup/R-C3D).  The purpose of this fork is to implement the R-C3D algorithm in terms of the [DIVA Framework]().  The DIVA Framework is an open source framework for deploying video activity recognition analytics in a multi-camera environment funded by [IARPA](https://www.iarpa.gov/). 

# Training on MEVA Datatset

Refer to the training readme in [experiments/virat/README.md](https://gitlab.kitware.com/kwiver/R-C3D/-/tree/dev/train-on-m1/experiments%2Fvirat)

## DIVA Framework Resources

* [DIVA Framework Github Repository](https://github.com/Kitware/DIVA) This is the main DIVA Framework site, all development of the framework happens here.
* [DIVA Framework Issue Tracker](https://github.com/Kitware/DIVA/issues)  Submit any bug reports or feature requests for the framework here.
* [DIVA Framework Main Documentation Page](https://kwiver-diva.readthedocs.io/en/latest/>) The source for the framework documentation is maintained in the Github repository using [Sphinx](http://www.sphinx-doc.org/en/master/)  A built version is maintained on [ReadTheDocs](https://readthedocs.org/).   A good place to get started in the documentation, after reading the [Introduction](https://kwiver-diva.readthedocs.io/en/latest/introduction.html) is the [UseCase](https://kwiver-diva.readthedocs.io/en/latest/usecases.html) section which will walk you though a number of typical use cases with the framework.

## Guide to this Adaptation

Detailed technical documentation on this adaption can be found at [this location](https://rc3d.readthedocs.io/en/kitware-master/index.html)

The Sprokit pipeline that runs the R-C3D process can be found in [pipelines/rc3d.pipe](https://gitlab.kitware.com/kwiver/R-C3D/blob/kitware/master/pipelines/rc3d.pipe)

The code that implements the core R-C3D Sprokit process can be found in [processes/rc3d_detector.py](https://gitlab.kitware.com/kwiver/R-C3D/blob/kitware/master/processes/rc3d_detector.py)

A [Dockerfile]() that creates a container in which the DIVA Framework is built and the R-C3D process runs can be found [docker/Dockerfile](https://gitlab.kitware.com/kwiver/R-C3D/blob/kitware/master/docker/Dockerfile)

## Adapting R-C3D to [actev diva_evaluation_cli](https://gitlab.kitware.com/actev/diva_evaluation_cli)

The baseline submission of [Actev sdl](https://actev.nist.gov/sdl) uses a prebuilt [docker container](gitlab.kitware.com:4567/kwiver/r-c3d/framework-cli:multi-gpu). The dockerfile used for building the container is available in [Dockerfile.wheel](https://gitlab.kitware.com/kwiver/R-C3D/blob/kitware/master/Dockerfile.wheel). The container uses [`chunk_runner.py`](https://gitlab.kitware.com/kwiver/R-C3D/blob/kitware/master/cli-helpers/chunk_runner.py) to run the rc3d pipeline using kwiver tools. For additional details about the modifications to diva_evaluation_cli refer to [kwiver/diva_evaluation_cli](https://gitlab.kitware.com/kwiver/diva_evaluation_cli/blob/master/README.md).
